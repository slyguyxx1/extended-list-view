﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using extendedListView.Models;

namespace extendedListView
{
	

	public partial class MoreItemInfoPage : ContentPage
	{
		public MoreItemInfoPage ()
		{
			InitializeComponent ();
		}

        public MoreItemInfoPage(ImageCellItems item)
        {
            InitializeComponent();

            BindingContext = item;
        }

        
        void Handle_url(object sender, EventArgs e)
        {

            var view = (ListView)sender;
            ImageCellItems itemSelect = (ImageCellItems)view.SelectedItem;
            var uri = new Uri(itemSelect.url);
            Device.OpenUri(uri);
            
        }
    }
}